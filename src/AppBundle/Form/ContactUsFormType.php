<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/12/16
 * Time: 11:36 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactUsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class, array('required' => false))
            ->add('email', EmailType::class)
            ->add('subject', ChoiceType::class, array(
                'choices' => array(
                    'report' => 'Report',
                    'communicate' => 'Communicate',
                    'other' => 'Other   '
                ),
                'required'    => false,
                'placeholder' => 'Choose subject',
                'empty_data'  => 'other'))
            ->add('message', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\ContactUs',
        ]);
    }


}