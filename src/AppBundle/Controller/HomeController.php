<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/5/16
 * Time: 10:08 PM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/home.html.twig', ['message' => "World"]);
    }
}