<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/12/16
 * Time: 11:44 PM
 */

namespace AppBundle\Dao;


class ContactUsDao extends BaseDao {

    public function saveContactUsDetails($contactUs)
    {
        try {
            $shipmentEm = $this->getEntityManager();
            $shipmentEm->persist($contactUs);
            $shipmentEm->flush();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}