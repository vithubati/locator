<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/12/16
 * Time: 11:46 PM
 */

namespace AppBundle\Dao;


class BaseDao
{
    protected function getEntityManager() {
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        return $em;
    }
}