<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/12/16
 * Time: 11:08 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\ContactUs;
use AppBundle\Form\ContactUsFormType;
use AppBundle\Service\ContactUsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ContactUsController extends Controller {
    private $contactUsService;

    public function getContactUsService() {
        if (!($this->contactUsService instanceof ContactUsService)) {
            $this->contactUsService = new ContactUsService();
        }
        return $this->contactUsService;
    }

    /**
     * @Route("/contactus", name="contactuspage")
     */
    public function indexAction(Request $request) {
        $contactUs = new ContactUs();
//        , array(
//            'action' => $this->generateUrl('contactus')
        $contactUsForm = $this->createForm(ContactUsFormType::class, $contactUs);
        $contactUsForm->handleRequest($request);

        if ($contactUsForm->isSubmitted() && $contactUsForm->isValid()) {
            $savedContactUs = $this->getContactUsService()->saveContactUsDetails($contactUs);
            if ($savedContactUs){
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Message successfully sent!');
                return $this->redirectToRoute('contactuspage');
            }else{
                $request->getSession()
                    ->getFlashBag()
                    ->add('failure', 'An Error occurred, Please Try again!')
                ;
                return $this->redirectToRoute('contactuspage');
            }
        }
        return $this->render('institute/contactus.html.twig', array(
            'contactUsForm' => $contactUsForm->createView()
        ));
    }
}