<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 12/12/16
 * Time: 11:44 PM
 */

namespace AppBundle\Service;


use AppBundle\Dao\ContactUsDao;

class ContactUsService
{
    private $contactUsDao;

    public function getContactUsDao() {
        if (is_null($this->contactUsDao)) {
            $this->contactUsDao = new ContactUsDao();
        }
        return $this->contactUsDao;
    }
    public function saveContactUsDetails($contactUsData) {
        return $this->getContactUsDao()->saveContactUsDetails($contactUsData);
    }
}